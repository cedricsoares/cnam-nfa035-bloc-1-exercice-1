package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;

/**
 * Interface de sérialisation d'iumages
 * @author Cédric Soares
 * @version 1.0 (Octobre 2021)
 */
public interface ImageSerializer {
    Object serialize(File image) throws IOException;

    Object deserialize(String encodedImage);
}
