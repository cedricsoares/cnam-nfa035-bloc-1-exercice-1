/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;


/**
 * Classe de sérialisation d'iumages implémente l'interface ImageSerializer
 * @see ImageSerializer
 * @author Cédric Soares
 * @version 1.0 (Octobre 2021)
 */
public class ImageSerializerBase64Impl implements ImageSerializer {

    /**
     *
     * @param image : Fichier image à sérialiser
     * @return dataString : Version serialisée de l'image sous forme de String
     * @throws IOException
     */
    @Override
    public String serialize(File image) throws IOException {
        Path path = image.toPath();
        byte[] data = Files.readAllBytes(path);
        String dataString = Base64.getEncoder().encodeToString(data);
        return dataString;
    }

    /**
     *
     * @param encodedImage : String serialisé d'une image
     * @return data : Un array de Bytes d'encodedImage désérialisé
     */
    @Override
    public byte[] deserialize(String encodedImage) {
        byte[] data = Base64.getDecoder().decode(encodedImage);
        return data;
    }
}
